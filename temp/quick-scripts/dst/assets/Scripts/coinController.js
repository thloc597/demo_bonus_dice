
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/coinController.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'fd575kB5TBADbsPff57ZrBl', 'coinController');
// Scripts/coinController.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var CoinController = /** @class */ (function (_super) {
    __extends(CoinController, _super);
    function CoinController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.coinTouch = null;
        return _this;
        // update (dt) {}
    }
    // LIFE-CYCLE CALLBACKS:
    CoinController.prototype.onBeginContact = function (contact, self, other) {
        if (other) {
            cc.audioEngine.play(this.coinTouch, false, 1);
            this.node.destroy();
        }
    };
    CoinController.prototype.onLoad = function () {
        var manager = cc.director.getCollisionManager();
        manager.enabled = true;
        manager.enabledDebugDraw = true;
        manager.enabledDrawBoundingBox = true;
        this.node.runAction(cc.repeatForever(cc.sequence(cc.scaleTo(0.5, -0.02, 0.02), cc.scaleTo(0.5, 0.02, 0.02))));
        this.node.zIndex = 1;
    };
    // onBeginContact (selfCollider: Collider2D, otherCollider: Collider2D, contact: IPhysics2DContact | null) {
    //     // will be called once when two colliders begin to contact
    //     console.log('onBeginContact');
    // }
    // onEndContact (selfCollider: Collider2D, otherCollider: Collider2D, contact: IPhysics2DContact | null) {
    //     // will be called once when the contact between two colliders just about to end.
    //     console.log('onEndContact');
    // }
    // onPreSolve (selfCollider: Collider2D, otherCollider: Collider2D, contact: IPhysics2DContact | null) {
    //     // will be called every time collider contact should be resolved
    //     console.log('onPreSolve');
    // }
    // onPostSolve (selfCollider: Collider2D, otherCollider: Collider2D, contact: IPhysics2DContact | null) {
    //     // will be called every time collider contact should be resolved
    //     console.log('onPostSolve');
    // }
    CoinController.prototype.start = function () {
    };
    __decorate([
        property(cc.AudioClip)
    ], CoinController.prototype, "coinTouch", void 0);
    CoinController = __decorate([
        ccclass
    ], CoinController);
    return CoinController;
}(cc.Component));
exports.default = CoinController;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcY29pbkNvbnRyb2xsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG9CQUFvQjtBQUNwQix3RUFBd0U7QUFDeEUsbUJBQW1CO0FBQ25CLGtGQUFrRjtBQUNsRiw4QkFBOEI7QUFDOUIsa0ZBQWtGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFNUUsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBNEMsa0NBQVk7SUFBeEQ7UUFBQSxxRUEwQ0M7UUF2Q0csZUFBUyxHQUFpQixJQUFJLENBQUM7O1FBc0MvQixpQkFBaUI7SUFDckIsQ0FBQztJQXJDRyx3QkFBd0I7SUFDeEIsdUNBQWMsR0FBZCxVQUFlLE9BQU8sRUFBRSxJQUFJLEVBQUUsS0FBSztRQUMvQixJQUFJLEtBQUssRUFBRTtZQUNQLEVBQUUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzlDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7U0FDdkI7SUFDTCxDQUFDO0lBQ0QsK0JBQU0sR0FBTjtRQUNJLElBQUksT0FBTyxHQUFHLEVBQUUsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztRQUNoRCxPQUFPLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztRQUN2QixPQUFPLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO1FBQ2hDLE9BQU8sQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUM7UUFDdEMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBRSxFQUFFLEVBQUUsQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUM5RyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUE7SUFFeEIsQ0FBQztJQUNELDRHQUE0RztJQUM1RyxpRUFBaUU7SUFDakUscUNBQXFDO0lBQ3JDLElBQUk7SUFDSiwwR0FBMEc7SUFDMUcsdUZBQXVGO0lBQ3ZGLG1DQUFtQztJQUNuQyxJQUFJO0lBQ0osd0dBQXdHO0lBQ3hHLHVFQUF1RTtJQUN2RSxpQ0FBaUM7SUFDakMsSUFBSTtJQUNKLHlHQUF5RztJQUN6Ryx1RUFBdUU7SUFDdkUsa0NBQWtDO0lBQ2xDLElBQUk7SUFDSiw4QkFBSyxHQUFMO0lBRUEsQ0FBQztJQXBDRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDO3FEQUNRO0lBSGQsY0FBYztRQURsQyxPQUFPO09BQ2EsY0FBYyxDQTBDbEM7SUFBRCxxQkFBQztDQTFDRCxBQTBDQyxDQTFDMkMsRUFBRSxDQUFDLFNBQVMsR0EwQ3ZEO2tCQTFDb0IsY0FBYyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIFR5cGVTY3JpcHQ6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxyXG4vLyBMZWFybiBBdHRyaWJ1dGU6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcclxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcclxuXHJcbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XHJcblxyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBDb2luQ29udHJvbGxlciBleHRlbmRzIGNjLkNvbXBvbmVudCB7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLkF1ZGlvQ2xpcClcclxuICAgIGNvaW5Ub3VjaDogY2MuQXVkaW9DbGlwID0gbnVsbDtcclxuXHJcbiAgICAvLyBMSUZFLUNZQ0xFIENBTExCQUNLUzpcclxuICAgIG9uQmVnaW5Db250YWN0KGNvbnRhY3QsIHNlbGYsIG90aGVyKSB7XHJcbiAgICAgICAgaWYgKG90aGVyKSB7XHJcbiAgICAgICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXkodGhpcy5jb2luVG91Y2gsIGZhbHNlLCAxKTtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLmRlc3Ryb3koKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBvbkxvYWQoKSB7XHJcbiAgICAgICAgdmFyIG1hbmFnZXIgPSBjYy5kaXJlY3Rvci5nZXRDb2xsaXNpb25NYW5hZ2VyKCk7XHJcbiAgICAgICAgbWFuYWdlci5lbmFibGVkID0gdHJ1ZTtcclxuICAgICAgICBtYW5hZ2VyLmVuYWJsZWREZWJ1Z0RyYXcgPSB0cnVlO1xyXG4gICAgICAgIG1hbmFnZXIuZW5hYmxlZERyYXdCb3VuZGluZ0JveCA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5ub2RlLnJ1bkFjdGlvbihjYy5yZXBlYXRGb3JldmVyKGNjLnNlcXVlbmNlKGNjLnNjYWxlVG8oMC41LCAtMC4wMiwgMC4wMiwpLCBjYy5zY2FsZVRvKDAuNSwgMC4wMiwgMC4wMikpKSlcclxuICAgICAgICB0aGlzLm5vZGUuekluZGV4ID0gMVxyXG5cclxuICAgIH1cclxuICAgIC8vIG9uQmVnaW5Db250YWN0IChzZWxmQ29sbGlkZXI6IENvbGxpZGVyMkQsIG90aGVyQ29sbGlkZXI6IENvbGxpZGVyMkQsIGNvbnRhY3Q6IElQaHlzaWNzMkRDb250YWN0IHwgbnVsbCkge1xyXG4gICAgLy8gICAgIC8vIHdpbGwgYmUgY2FsbGVkIG9uY2Ugd2hlbiB0d28gY29sbGlkZXJzIGJlZ2luIHRvIGNvbnRhY3RcclxuICAgIC8vICAgICBjb25zb2xlLmxvZygnb25CZWdpbkNvbnRhY3QnKTtcclxuICAgIC8vIH1cclxuICAgIC8vIG9uRW5kQ29udGFjdCAoc2VsZkNvbGxpZGVyOiBDb2xsaWRlcjJELCBvdGhlckNvbGxpZGVyOiBDb2xsaWRlcjJELCBjb250YWN0OiBJUGh5c2ljczJEQ29udGFjdCB8IG51bGwpIHtcclxuICAgIC8vICAgICAvLyB3aWxsIGJlIGNhbGxlZCBvbmNlIHdoZW4gdGhlIGNvbnRhY3QgYmV0d2VlbiB0d28gY29sbGlkZXJzIGp1c3QgYWJvdXQgdG8gZW5kLlxyXG4gICAgLy8gICAgIGNvbnNvbGUubG9nKCdvbkVuZENvbnRhY3QnKTtcclxuICAgIC8vIH1cclxuICAgIC8vIG9uUHJlU29sdmUgKHNlbGZDb2xsaWRlcjogQ29sbGlkZXIyRCwgb3RoZXJDb2xsaWRlcjogQ29sbGlkZXIyRCwgY29udGFjdDogSVBoeXNpY3MyRENvbnRhY3QgfCBudWxsKSB7XHJcbiAgICAvLyAgICAgLy8gd2lsbCBiZSBjYWxsZWQgZXZlcnkgdGltZSBjb2xsaWRlciBjb250YWN0IHNob3VsZCBiZSByZXNvbHZlZFxyXG4gICAgLy8gICAgIGNvbnNvbGUubG9nKCdvblByZVNvbHZlJyk7XHJcbiAgICAvLyB9XHJcbiAgICAvLyBvblBvc3RTb2x2ZSAoc2VsZkNvbGxpZGVyOiBDb2xsaWRlcjJELCBvdGhlckNvbGxpZGVyOiBDb2xsaWRlcjJELCBjb250YWN0OiBJUGh5c2ljczJEQ29udGFjdCB8IG51bGwpIHtcclxuICAgIC8vICAgICAvLyB3aWxsIGJlIGNhbGxlZCBldmVyeSB0aW1lIGNvbGxpZGVyIGNvbnRhY3Qgc2hvdWxkIGJlIHJlc29sdmVkXHJcbiAgICAvLyAgICAgY29uc29sZS5sb2coJ29uUG9zdFNvbHZlJyk7XHJcbiAgICAvLyB9XHJcbiAgICBzdGFydCgpIHtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgLy8gdXBkYXRlIChkdCkge31cclxufVxyXG4iXX0=
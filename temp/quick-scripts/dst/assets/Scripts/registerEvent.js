
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/registerEvent.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '03374pJf5NI4r2wxTYyBB43', 'registerEvent');
// Scripts/registerEvent.ts

var EventEmitter = require('events');
var mEmitter = /** @class */ (function () {
    function mEmitter() {
        this._emiter = new EventEmitter();
        this._emiter.setMaxListeners(100);
    }
    mEmitter.prototype.emit = function () {
        var _a;
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        (_a = this._emiter).emit.apply(_a, args);
    };
    mEmitter.prototype.registerEvent = function (event, listener) {
        this._emiter.on(event, listener);
    };
    mEmitter.prototype.registerOnce = function (event, listener) {
        this._emiter.once(event, listener);
    };
    mEmitter.prototype.removeEvent = function (event, listener) {
        this._emiter.removeListener(event, listener);
    };
    mEmitter.prototype.destroy = function () {
        this._emiter.removeAllListeners();
        this._emiter = null;
        mEmitter.instance = null;
    };
    return mEmitter;
}());
mEmitter.instance = null;
module.exports = mEmitter;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xccmVnaXN0ZXJFdmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7QUFBQSxJQUFNLFlBQVksR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7QUFDdkM7SUFDSTtRQUNJLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUNsQyxJQUFJLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUN0QyxDQUFDO0lBQ0QsdUJBQUksR0FBSjs7UUFBSyxjQUFPO2FBQVAsVUFBTyxFQUFQLHFCQUFPLEVBQVAsSUFBTztZQUFQLHlCQUFPOztRQUNSLENBQUEsS0FBQSxJQUFJLENBQUMsT0FBTyxDQUFBLENBQUMsSUFBSSxXQUFJLElBQUksRUFBRTtJQUMvQixDQUFDO0lBQ0QsZ0NBQWEsR0FBYixVQUFjLEtBQUssRUFBRSxRQUFRO1FBQ3pCLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLEtBQUssRUFBRSxRQUFRLENBQUMsQ0FBQztJQUNyQyxDQUFDO0lBQ0QsK0JBQVksR0FBWixVQUFhLEtBQUssRUFBRSxRQUFRO1FBQ3hCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxRQUFRLENBQUMsQ0FBQztJQUN2QyxDQUFDO0lBQ0QsOEJBQVcsR0FBWCxVQUFZLEtBQUssRUFBRSxRQUFRO1FBQ3ZCLElBQUksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLEtBQUssRUFBRSxRQUFRLENBQUMsQ0FBQztJQUNqRCxDQUFDO0lBQ0QsMEJBQU8sR0FBUDtRQUNJLElBQUksQ0FBQyxPQUFPLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUNsQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztRQUNwQixRQUFRLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztJQUM3QixDQUFDO0lBQ0wsZUFBQztBQUFELENBdEJBLEFBc0JDLElBQUE7QUFDRCxRQUFRLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztBQUN6QixNQUFNLENBQUMsT0FBTyxHQUFHLFFBQVEsQ0FBQyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImNvbnN0IEV2ZW50RW1pdHRlciA9IHJlcXVpcmUoJ2V2ZW50cycpO1xyXG5jbGFzcyBtRW1pdHRlciB7XHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICB0aGlzLl9lbWl0ZXIgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcbiAgICAgICAgdGhpcy5fZW1pdGVyLnNldE1heExpc3RlbmVycygxMDApO1xyXG4gICAgfVxyXG4gICAgZW1pdCguLi5hcmdzKSB7XHJcbiAgICAgICAgdGhpcy5fZW1pdGVyLmVtaXQoLi4uYXJncyk7XHJcbiAgICB9XHJcbiAgICByZWdpc3RlckV2ZW50KGV2ZW50LCBsaXN0ZW5lcikge1xyXG4gICAgICAgIHRoaXMuX2VtaXRlci5vbihldmVudCwgbGlzdGVuZXIpO1xyXG4gICAgfVxyXG4gICAgcmVnaXN0ZXJPbmNlKGV2ZW50LCBsaXN0ZW5lcikge1xyXG4gICAgICAgIHRoaXMuX2VtaXRlci5vbmNlKGV2ZW50LCBsaXN0ZW5lcik7XHJcbiAgICB9XHJcbiAgICByZW1vdmVFdmVudChldmVudCwgbGlzdGVuZXIpIHtcclxuICAgICAgICB0aGlzLl9lbWl0ZXIucmVtb3ZlTGlzdGVuZXIoZXZlbnQsIGxpc3RlbmVyKTtcclxuICAgIH1cclxuICAgIGRlc3Ryb3koKSB7XHJcbiAgICAgICAgdGhpcy5fZW1pdGVyLnJlbW92ZUFsbExpc3RlbmVycygpO1xyXG4gICAgICAgIHRoaXMuX2VtaXRlciA9IG51bGw7XHJcbiAgICAgICAgbUVtaXR0ZXIuaW5zdGFuY2UgPSBudWxsO1xyXG4gICAgfVxyXG59XHJcbm1FbWl0dGVyLmluc3RhbmNlID0gbnVsbDtcclxubW9kdWxlLmV4cG9ydHMgPSBtRW1pdHRlcjsiXX0=
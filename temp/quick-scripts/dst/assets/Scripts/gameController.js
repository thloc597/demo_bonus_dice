
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/gameController.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'bda65V044ZBJrYIaWWvREjR', 'gameController');
// Scripts/gameController.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var GameController = /** @class */ (function (_super) {
    __extends(GameController, _super);
    function GameController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.posX = [];
        _this.anim = null;
        _this.player = null;
        _this.walkSound = null;
        _this.bgMusic = null;
        _this.coinSound = null;
        _this.touchSound = null;
        _this.shootSound = null;
        _this.jumpSound = null;
        _this.portalSound = null;
        _this.bullet = null;
        _this.coin = null;
        _this.gameOverPopup = null;
        _this.parentLeft = null;
        _this.parentRight = null;
        _this.isGround = true;
        _this.gameOverMusic = null;
        return _this;
        // update (dt) {}
    }
    // LIFE-CYCLE CALLBACKS:
    GameController.prototype.createArrPos = function (min, max) {
        for (var i = min; i <= max; i += 20) {
            this.posX.push(i);
        }
        this.posLength = this.posX.length;
        return this.posX.length;
    };
    GameController.prototype.onLoad = function () {
        var _this = this;
        cc.audioEngine.play(this.bgMusic, true, 0);
        this.createArrPos(-510, 470);
        this.setEventListenner();
        var physicsManager = cc.director.getPhysicsManager();
        physicsManager.enabled = true;
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        this.anim.setAnimation(0, 'portal', false);
        this.anim.addAnimation(0, 'idle', true);
        for (var i = 0; i < this.countCoin; i++) {
            this.initCoin();
        }
        var manager = cc.director.getCollisionManager();
        manager.enabled = true;
        manager.enabledDebugDraw = true;
        this.anim.setEventListener(function (entry, event) {
            var data = event.data;
            if (data.name == 'footstep') {
                cc.audioEngine.play(_this.walkSound, false, 1);
            }
        });
    };
    GameController.prototype.start = function () {
    };
    GameController.prototype.onKeyDown = function (event) {
        switch (event.keyCode) {
            case cc.macro.KEY.left:
                this.moveLeft();
                break;
            case cc.macro.KEY.right:
                this.moveRight();
                break;
            case cc.macro.KEY.x:
                this.moveJump();
                break;
            case cc.macro.KEY.z:
                this.shoot();
                break;
        }
    };
    GameController.prototype.initCoin = function () {
        var posX = this.posX[(Math.floor(Math.random() * (this.posLength - 1 - 0 + 1)))];
        var posY = Math.floor(Math.random() * (-175 - -260 + 1)) + -260;
        var _coin = cc.instantiate(this.coin);
        _coin.parent = this.node;
        _coin.x = posX;
        _coin.y = posY;
    };
    GameController.prototype.initBullet = function () {
        cc.error("Init");
        var bullet = cc.instantiate(this.bullet);
        if (this.player.getChildByName('player').scaleX > 0) {
            bullet.parent = this.parentRight;
            bullet.runAction(cc.repeatForever(cc.moveBy(0.1, cc.v2(this.bulletSpeed, 0))));
        }
        else {
            bullet.parent = this.parentLeft;
            bullet.runAction(cc.repeatForever(cc.moveBy(0.1, cc.v2(-this.bulletSpeed, 0))));
        }
    };
    GameController.prototype.shoot = function () {
        this.anim.setAnimation(0, 'shoot', false);
        this.anim.addAnimation(0, 'idle', true);
    };
    GameController.prototype.moveLeft = function () {
        var _this = this;
        this.anim.setAnimation(0, 'walk', false);
        this.player.getChildByName('player').scaleX = -0.1;
        // this.player.getComponent(cc.RigidBody).applyForceToCenter(cc.v2(20,0), true);
        this.player.runAction(cc.sequence(cc.moveBy(this.duration, cc.v2(-this.moveSpeed, 0)), cc.callFunc(function () {
            _this.anim.addAnimation(0, 'idle', true);
        })));
    };
    GameController.prototype.moveRight = function () {
        var _this = this;
        this.anim.setAnimation(0, 'walk', false);
        this.player.getChildByName('player').scaleX = 0.1;
        this.player.runAction(cc.sequence(cc.moveBy(this.duration, cc.v2(this.moveSpeed, 0)), cc.callFunc(function () {
            _this.anim.addAnimation(0, 'idle', true);
        })));
    };
    GameController.prototype.moveJump = function () {
        var _this = this;
        if (this.isGround == true) {
            this.isGround = false;
            this.anim.setAnimation(0, 'jump', false);
            var movePosition = void 0;
            if (this.player.getChildByName('player').scaleX > 0) {
                movePosition = this.moveSpeed;
            }
            else {
                movePosition = -this.moveSpeed;
            }
            this.player.runAction(cc.sequence(cc.moveBy(this.duration, cc.v2(movePosition, this.jumpSpeed)), cc.moveBy(this.duration, cc.v2(movePosition, -this.jumpSpeed)), cc.callFunc(function () {
                _this.isGround = true;
                _this.anim.addAnimation(0, 'idle', true);
            })));
        }
        else {
            return;
        }
    };
    GameController.prototype.setEventListenner = function () {
        var _this = this;
        this.anim.setStartListener(function (event) {
            var name = event.animation.name;
            switch (name) {
                case 'portal': {
                    _this.scheduleOnce(function () {
                        cc.audioEngine.play(_this.portalSound, false, 1);
                    }, 0.5);
                    _this.scheduleOnce(function () {
                        cc.audioEngine.play(_this.portalSound, false, 1);
                    }, 2.5);
                    break;
                }
                case 'walk': {
                    break;
                }
                case 'shoot': {
                    cc.audioEngine.play(_this.shootSound, false, 1);
                    _this.initBullet();
                    break;
                }
                case 'jump': {
                    cc.audioEngine.play(_this.jumpSound, false, 1);
                    break;
                }
                case 'death': {
                    cc.audioEngine.play(_this.gameOverMusic, false, 1);
                    cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, _this.onKeyDown, _this);
                    break;
                }
            }
        });
    };
    GameController.prototype.gameOver = function () {
        if (this.player.y < 68) {
            cc.audioEngine.play(this.gameOverMusic, false, 1);
        }
    };
    __decorate([
        property
    ], GameController.prototype, "moveSpeed", void 0);
    __decorate([
        property
    ], GameController.prototype, "countCoin", void 0);
    __decorate([
        property
    ], GameController.prototype, "bulletSpeed", void 0);
    __decorate([
        property
    ], GameController.prototype, "jumpSpeed", void 0);
    __decorate([
        property
    ], GameController.prototype, "duration", void 0);
    __decorate([
        property(sp.Skeleton)
    ], GameController.prototype, "anim", void 0);
    __decorate([
        property(cc.Node)
    ], GameController.prototype, "player", void 0);
    __decorate([
        property(cc.AudioClip)
    ], GameController.prototype, "walkSound", void 0);
    __decorate([
        property(cc.AudioClip)
    ], GameController.prototype, "bgMusic", void 0);
    __decorate([
        property(cc.AudioClip)
    ], GameController.prototype, "coinSound", void 0);
    __decorate([
        property(cc.AudioClip)
    ], GameController.prototype, "touchSound", void 0);
    __decorate([
        property(cc.AudioClip)
    ], GameController.prototype, "shootSound", void 0);
    __decorate([
        property(cc.AudioClip)
    ], GameController.prototype, "jumpSound", void 0);
    __decorate([
        property(cc.AudioClip)
    ], GameController.prototype, "portalSound", void 0);
    __decorate([
        property(cc.Prefab)
    ], GameController.prototype, "bullet", void 0);
    __decorate([
        property(cc.Prefab)
    ], GameController.prototype, "coin", void 0);
    __decorate([
        property(cc.Node)
    ], GameController.prototype, "gameOverPopup", void 0);
    __decorate([
        property(cc.Node)
    ], GameController.prototype, "parentLeft", void 0);
    __decorate([
        property(cc.Node)
    ], GameController.prototype, "parentRight", void 0);
    __decorate([
        property(cc.AudioClip)
    ], GameController.prototype, "gameOverMusic", void 0);
    GameController = __decorate([
        ccclass
    ], GameController);
    return GameController;
}(cc.Component));
exports.default = GameController;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcZ2FtZUNvbnRyb2xsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG9CQUFvQjtBQUNwQix3RUFBd0U7QUFDeEUsbUJBQW1CO0FBQ25CLGtGQUFrRjtBQUNsRiw4QkFBOEI7QUFDOUIsa0ZBQWtGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFHNUUsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBNEMsa0NBQVk7SUFBeEQ7UUFBQSxxRUEwTUM7UUFwTVcsVUFBSSxHQUFHLEVBQUUsQ0FBQTtRQVNqQixVQUFJLEdBQWdCLElBQUksQ0FBQztRQUV6QixZQUFNLEdBQVksSUFBSSxDQUFDO1FBRXZCLGVBQVMsR0FBaUIsSUFBSSxDQUFDO1FBRS9CLGFBQU8sR0FBaUIsSUFBSSxDQUFDO1FBRTdCLGVBQVMsR0FBaUIsSUFBSSxDQUFDO1FBRS9CLGdCQUFVLEdBQWlCLElBQUksQ0FBQztRQUVoQyxnQkFBVSxHQUFpQixJQUFJLENBQUM7UUFFaEMsZUFBUyxHQUFpQixJQUFJLENBQUM7UUFFL0IsaUJBQVcsR0FBaUIsSUFBSSxDQUFDO1FBRWpDLFlBQU0sR0FBYyxJQUFJLENBQUM7UUFFekIsVUFBSSxHQUFjLElBQUksQ0FBQztRQUV2QixtQkFBYSxHQUFZLElBQUksQ0FBQztRQUU5QixnQkFBVSxHQUFZLElBQUksQ0FBQztRQUUzQixpQkFBVyxHQUFZLElBQUksQ0FBQztRQUNwQixjQUFRLEdBQVksSUFBSSxDQUFDO1FBRWpDLG1CQUFhLEdBQWlCLElBQUksQ0FBQzs7UUE2Sm5DLGlCQUFpQjtJQUNyQixDQUFDO0lBN0pHLHdCQUF3QjtJQUd4QixxQ0FBWSxHQUFaLFVBQWEsR0FBVyxFQUFFLEdBQVc7UUFDakMsS0FBSSxJQUFJLENBQUMsR0FBRyxHQUFHLEVBQUUsQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDLElBQUcsRUFBRSxFQUFDO1lBQzlCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ3JCO1FBQ0QsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQTtRQUNqQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBQzVCLENBQUM7SUFDRCwrQkFBTSxHQUFOO1FBQUEsaUJBc0JDO1FBckJHLEVBQUUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzNDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDN0IsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUE7UUFDeEIsSUFBSSxjQUFjLEdBQUcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBQ3JELGNBQWMsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQzlCLEVBQUUsQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzNFLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsRUFBRSxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDM0MsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQztRQUN4QyxLQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLEVBQUUsRUFBQztZQUNuQyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7U0FDbkI7UUFDRCxJQUFJLE9BQU8sR0FBRyxFQUFFLENBQUMsUUFBUSxDQUFDLG1CQUFtQixFQUFFLENBQUM7UUFDaEQsT0FBTyxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDdkIsT0FBTyxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztRQUNoQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFVBQUMsS0FBSyxFQUFFLEtBQUs7WUFDNUIsSUFBQSxJQUFJLEdBQUssS0FBSyxLQUFWLENBQVc7WUFDdkIsSUFBRyxJQUFJLENBQUMsSUFBSSxJQUFJLFVBQVUsRUFBQztnQkFDdkIsRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLFNBQVMsRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7YUFDakQ7UUFFTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFDRCw4QkFBSyxHQUFMO0lBRUEsQ0FBQztJQUVELGtDQUFTLEdBQVQsVUFBVSxLQUFLO1FBQ1gsUUFBUSxLQUFLLENBQUMsT0FBTyxFQUFFO1lBQ25CLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsSUFBSTtnQkFDbEIsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFBO2dCQUNmLE1BQU07WUFDVixLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEtBQUs7Z0JBQ25CLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztnQkFDakIsTUFBTTtZQUNWLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDZixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ2hCLE1BQU07WUFDVixLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ2YsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUNiLE1BQU07U0FDYjtJQUNMLENBQUM7SUFDRCxpQ0FBUSxHQUFSO1FBQ0ksSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQy9FLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQztRQUM5RCxJQUFJLEtBQUssR0FBRyxFQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0QyxLQUFLLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7UUFDekIsS0FBSyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUM7UUFDZixLQUFLLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQztJQUNuQixDQUFDO0lBQ0QsbUNBQVUsR0FBVjtRQUNJLEVBQUUsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUE7UUFDaEIsSUFBSSxNQUFNLEdBQUcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDekMsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ2pELE1BQU0sQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztZQUNqQyxNQUFNLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ2xGO2FBQ0k7WUFDRCxNQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7WUFDaEMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ25GO0lBRUwsQ0FBQztJQUNELDhCQUFLLEdBQUw7UUFDSSxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLEVBQUUsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQzFDLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsRUFBRSxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDNUMsQ0FBQztJQUNELGlDQUFRLEdBQVI7UUFBQSxpQkFPQztRQU5HLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsRUFBRSxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDekMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsR0FBRyxDQUFDO1FBQ25ELGdGQUFnRjtRQUNoRixJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxRQUFRLENBQUM7WUFDL0YsS0FBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQztRQUM1QyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDVCxDQUFDO0lBQ0Qsa0NBQVMsR0FBVDtRQUFBLGlCQU1DO1FBTEcsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxFQUFFLE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDO1FBQ2xELElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxRQUFRLENBQUM7WUFDOUYsS0FBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQztRQUM1QyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDVCxDQUFDO0lBQ0QsaUNBQVEsR0FBUjtRQUFBLGlCQXNCQztRQXJCRyxJQUFHLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxFQUFDO1lBQ3JCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1lBQ3RCLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsRUFBRSxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUE7WUFDeEMsSUFBSSxZQUFZLFNBQUEsQ0FBQztZQUNqQixJQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUM7Z0JBQy9DLFlBQVksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO2FBQ2pDO2lCQUNHO2dCQUNBLFlBQVksR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7YUFDbEM7WUFDRCxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUNqQyxFQUFFLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQzdELEVBQUUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLFlBQVksRUFBRSxDQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUMvRCxFQUFFLENBQUMsUUFBUSxDQUFDO2dCQUNSLEtBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO2dCQUNyQixLQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLEVBQUUsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQzVDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtTQUNQO2FBQ0c7WUFDQSxPQUFPO1NBQ1Y7SUFDTCxDQUFDO0lBQ0QsMENBQWlCLEdBQWpCO1FBQUEsaUJBZ0NDO1FBL0JHLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsVUFBQyxLQUFLO1lBQzdCLElBQU0sSUFBSSxHQUFHLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDO1lBQ2xDLFFBQVEsSUFBSSxFQUFFO2dCQUNWLEtBQUssUUFBUSxDQUFDLENBQUM7b0JBQ1gsS0FBSSxDQUFDLFlBQVksQ0FBQzt3QkFDZCxFQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsV0FBVyxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDcEQsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFBO29CQUNQLEtBQUksQ0FBQyxZQUFZLENBQUM7d0JBQ2QsRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLFdBQVcsRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQ3BELENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQTtvQkFDUCxNQUFNO2lCQUNUO2dCQUNELEtBQUssTUFBTSxDQUFDLENBQUM7b0JBQ1QsTUFBTTtpQkFDVDtnQkFDRCxLQUFLLE9BQU8sQ0FBQyxDQUFDO29CQUNWLEVBQUUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxVQUFVLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUMvQyxLQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7b0JBQ2xCLE1BQU07aUJBQ1Q7Z0JBQ0QsS0FBSyxNQUFNLENBQUMsQ0FBQTtvQkFDUixFQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsU0FBUyxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDOUMsTUFBTTtpQkFDVDtnQkFDRCxLQUFLLE9BQU8sQ0FBQyxDQUFBO29CQUNULEVBQUUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxhQUFhLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUNsRCxFQUFFLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsS0FBSSxDQUFDLFNBQVMsRUFBRSxLQUFJLENBQUMsQ0FBQztvQkFDNUUsTUFBTTtpQkFDVDthQUNKO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBQ0QsaUNBQVEsR0FBUjtRQUNJLElBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUcsRUFBRSxFQUFDO1lBQ2xCLEVBQUUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ3JEO0lBQ0wsQ0FBQztJQW5NRDtRQURDLFFBQVE7cURBQ2lCO0lBRTFCO1FBREMsUUFBUTtxREFDaUI7SUFJMUI7UUFEQyxRQUFRO3VEQUNrQjtJQUUzQjtRQURDLFFBQVE7cURBQ2dCO0lBRXpCO1FBREMsUUFBUTtvREFDZTtJQUV4QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDO2dEQUNHO0lBRXpCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7a0RBQ0s7SUFFdkI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQztxREFDUTtJQUUvQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDO21EQUNNO0lBRTdCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUM7cURBQ1E7SUFFL0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQztzREFDUztJQUVoQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDO3NEQUNTO0lBRWhDO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUM7cURBQ1E7SUFFL0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQzt1REFDVTtJQUVqQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDO2tEQUNLO0lBRXpCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7Z0RBQ0c7SUFFdkI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzt5REFDWTtJQUU5QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO3NEQUNTO0lBRTNCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7dURBQ1U7SUFHNUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQzt5REFDWTtJQTVDbEIsY0FBYztRQURsQyxPQUFPO09BQ2EsY0FBYyxDQTBNbEM7SUFBRCxxQkFBQztDQTFNRCxBQTBNQyxDQTFNMkMsRUFBRSxDQUFDLFNBQVMsR0EwTXZEO2tCQTFNb0IsY0FBYyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIFR5cGVTY3JpcHQ6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxyXG4vLyBMZWFybiBBdHRyaWJ1dGU6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcclxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcclxuXHJcbmltcG9ydCBDb2luQ29udHJvbGxlciBmcm9tIFwiLi9jb2luQ29udHJvbGxlclwiO1xyXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgR2FtZUNvbnRyb2xsZXIgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xyXG5cclxuICAgIEBwcm9wZXJ0eVxyXG4gICAgcHJpdmF0ZSBtb3ZlU3BlZWQ6IG51bWJlcjtcclxuICAgIEBwcm9wZXJ0eVxyXG4gICAgcHJpdmF0ZSBjb3VudENvaW46IG51bWJlcjtcclxuICAgIHByaXZhdGUgcG9zWCA9IFtdXHJcbiAgICBwcml2YXRlIHBvc0xlbmd0aDogbnVtYmVyO1xyXG4gICAgQHByb3BlcnR5XHJcbiAgICBwcml2YXRlIGJ1bGxldFNwZWVkOiBudW1iZXJcclxuICAgIEBwcm9wZXJ0eVxyXG4gICAgcHJpdmF0ZSBqdW1wU3BlZWQ6IG51bWJlclxyXG4gICAgQHByb3BlcnR5XHJcbiAgICBwcml2YXRlIGR1cmF0aW9uOiBudW1iZXJcclxuICAgIEBwcm9wZXJ0eShzcC5Ta2VsZXRvbilcclxuICAgIGFuaW06IHNwLlNrZWxldG9uID0gbnVsbDtcclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgcGxheWVyOiBjYy5Ob2RlID0gbnVsbDtcclxuICAgIEBwcm9wZXJ0eShjYy5BdWRpb0NsaXApXHJcbiAgICB3YWxrU291bmQ6IGNjLkF1ZGlvQ2xpcCA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoY2MuQXVkaW9DbGlwKVxyXG4gICAgYmdNdXNpYzogY2MuQXVkaW9DbGlwID0gbnVsbDtcclxuICAgIEBwcm9wZXJ0eShjYy5BdWRpb0NsaXApXHJcbiAgICBjb2luU291bmQ6IGNjLkF1ZGlvQ2xpcCA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoY2MuQXVkaW9DbGlwKVxyXG4gICAgdG91Y2hTb3VuZDogY2MuQXVkaW9DbGlwID0gbnVsbDtcclxuICAgIEBwcm9wZXJ0eShjYy5BdWRpb0NsaXApXHJcbiAgICBzaG9vdFNvdW5kOiBjYy5BdWRpb0NsaXAgPSBudWxsO1xyXG4gICAgQHByb3BlcnR5KGNjLkF1ZGlvQ2xpcClcclxuICAgIGp1bXBTb3VuZDogY2MuQXVkaW9DbGlwID0gbnVsbDtcclxuICAgIEBwcm9wZXJ0eShjYy5BdWRpb0NsaXApXHJcbiAgICBwb3J0YWxTb3VuZDogY2MuQXVkaW9DbGlwID0gbnVsbDtcclxuICAgIEBwcm9wZXJ0eShjYy5QcmVmYWIpXHJcbiAgICBidWxsZXQ6IGNjLlByZWZhYiA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoY2MuUHJlZmFiKVxyXG4gICAgY29pbjogY2MuUHJlZmFiID0gbnVsbDtcclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgZ2FtZU92ZXJQb3B1cDogY2MuTm9kZSA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIHBhcmVudExlZnQ6IGNjLk5vZGUgPSBudWxsO1xyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBwYXJlbnRSaWdodDogY2MuTm9kZSA9IG51bGw7XHJcbiAgICBwcml2YXRlIGlzR3JvdW5kOiBib29sZWFuID0gdHJ1ZTtcclxuICAgIEBwcm9wZXJ0eShjYy5BdWRpb0NsaXApXHJcbiAgICBnYW1lT3Zlck11c2ljOiBjYy5BdWRpb0NsaXAgPSBudWxsO1xyXG4gICAgLy8gTElGRS1DWUNMRSBDQUxMQkFDS1M6XHJcblxyXG4gICAgXHJcbiAgICBjcmVhdGVBcnJQb3MobWluOiBudW1iZXIsIG1heDogbnVtYmVyKXtcclxuICAgICAgICBmb3IobGV0IGkgPSBtaW47IGkgPD0gbWF4OyBpKz0gMjApe1xyXG4gICAgICAgICAgICB0aGlzLnBvc1gucHVzaChpKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5wb3NMZW5ndGggPSB0aGlzLnBvc1gubGVuZ3RoXHJcbiAgICAgICAgcmV0dXJuIHRoaXMucG9zWC5sZW5ndGg7XHJcbiAgICB9XHJcbiAgICBvbkxvYWQoKSB7XHJcbiAgICAgICAgY2MuYXVkaW9FbmdpbmUucGxheSh0aGlzLmJnTXVzaWMsIHRydWUsIDApO1xyXG4gICAgICAgIHRoaXMuY3JlYXRlQXJyUG9zKC01MTAsIDQ3MCk7XHJcbiAgICAgICAgdGhpcy5zZXRFdmVudExpc3Rlbm5lcigpXHJcbiAgICAgICAgbGV0IHBoeXNpY3NNYW5hZ2VyID0gY2MuZGlyZWN0b3IuZ2V0UGh5c2ljc01hbmFnZXIoKTtcclxuICAgICAgICBwaHlzaWNzTWFuYWdlci5lbmFibGVkID0gdHJ1ZTtcclxuICAgICAgICBjYy5zeXN0ZW1FdmVudC5vbihjYy5TeXN0ZW1FdmVudC5FdmVudFR5cGUuS0VZX0RPV04sIHRoaXMub25LZXlEb3duLCB0aGlzKTtcclxuICAgICAgICB0aGlzLmFuaW0uc2V0QW5pbWF0aW9uKDAsICdwb3J0YWwnLCBmYWxzZSk7XHJcbiAgICAgICAgdGhpcy5hbmltLmFkZEFuaW1hdGlvbigwLCAnaWRsZScsIHRydWUpO1xyXG4gICAgICAgIGZvcihsZXQgaSA9IDA7IGkgPCB0aGlzLmNvdW50Q29pbjsgaSsrKXtcclxuICAgICAgICAgICAgdGhpcy5pbml0Q29pbigpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBsZXQgbWFuYWdlciA9IGNjLmRpcmVjdG9yLmdldENvbGxpc2lvbk1hbmFnZXIoKTtcclxuICAgICAgICBtYW5hZ2VyLmVuYWJsZWQgPSB0cnVlO1xyXG4gICAgICAgIG1hbmFnZXIuZW5hYmxlZERlYnVnRHJhdyA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5hbmltLnNldEV2ZW50TGlzdGVuZXIoKGVudHJ5LCBldmVudCkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCB7IGRhdGEgfSA9IGV2ZW50O1xyXG4gICAgICAgICAgICBpZihkYXRhLm5hbWUgPT0gJ2Zvb3RzdGVwJyl7XHJcbiAgICAgICAgICAgICAgICBjYy5hdWRpb0VuZ2luZS5wbGF5KHRoaXMud2Fsa1NvdW5kLCBmYWxzZSwgMSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICBzdGFydCgpIHtcclxuXHJcbiAgICB9XHJcbiAgICBcclxuICAgIG9uS2V5RG93bihldmVudCkge1xyXG4gICAgICAgIHN3aXRjaCAoZXZlbnQua2V5Q29kZSkge1xyXG4gICAgICAgICAgICBjYXNlIGNjLm1hY3JvLktFWS5sZWZ0OlxyXG4gICAgICAgICAgICAgICAgdGhpcy5tb3ZlTGVmdCgpXHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBjYy5tYWNyby5LRVkucmlnaHQ6XHJcbiAgICAgICAgICAgICAgICB0aGlzLm1vdmVSaWdodCgpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgY2MubWFjcm8uS0VZLng6XHJcbiAgICAgICAgICAgICAgICB0aGlzLm1vdmVKdW1wKCk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBjYy5tYWNyby5LRVkuejpcclxuICAgICAgICAgICAgICAgIHRoaXMuc2hvb3QoKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIGluaXRDb2luKCl7XHJcbiAgICAgICAgbGV0IHBvc1ggPSB0aGlzLnBvc1hbKE1hdGguZmxvb3IoTWF0aC5yYW5kb20oKSoodGhpcy5wb3NMZW5ndGggLSAxIC0gMCArIDEpKSldO1xyXG4gICAgICAgIGxldCBwb3NZID0gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpKigtMTc1IC0gLTI2MCArIDEpKSArIC0yNjA7XHJcbiAgICAgICAgbGV0IF9jb2luID0gY2MuaW5zdGFudGlhdGUodGhpcy5jb2luKTtcclxuICAgICAgICBfY29pbi5wYXJlbnQgPSB0aGlzLm5vZGU7XHJcbiAgICAgICAgX2NvaW4ueCA9IHBvc1g7XHJcbiAgICAgICAgX2NvaW4ueSA9IHBvc1k7XHJcbiAgICB9XHJcbiAgICBpbml0QnVsbGV0KCkge1xyXG4gICAgICAgIGNjLmVycm9yKFwiSW5pdFwiKVxyXG4gICAgICAgIGxldCBidWxsZXQgPSBjYy5pbnN0YW50aWF0ZSh0aGlzLmJ1bGxldCk7XHJcbiAgICAgICAgaWYgKHRoaXMucGxheWVyLmdldENoaWxkQnlOYW1lKCdwbGF5ZXInKS5zY2FsZVggPiAwKSB7XHJcbiAgICAgICAgICAgIGJ1bGxldC5wYXJlbnQgPSB0aGlzLnBhcmVudFJpZ2h0O1xyXG4gICAgICAgICAgICBidWxsZXQucnVuQWN0aW9uKGNjLnJlcGVhdEZvcmV2ZXIoY2MubW92ZUJ5KDAuMSwgY2MudjIodGhpcy5idWxsZXRTcGVlZCwgMCkpKSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICBidWxsZXQucGFyZW50ID0gdGhpcy5wYXJlbnRMZWZ0O1xyXG4gICAgICAgICAgICBidWxsZXQucnVuQWN0aW9uKGNjLnJlcGVhdEZvcmV2ZXIoY2MubW92ZUJ5KDAuMSwgY2MudjIoLXRoaXMuYnVsbGV0U3BlZWQsIDApKSkpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICB9XHJcbiAgICBzaG9vdCgpIHtcclxuICAgICAgICB0aGlzLmFuaW0uc2V0QW5pbWF0aW9uKDAsICdzaG9vdCcsIGZhbHNlKTtcclxuICAgICAgICB0aGlzLmFuaW0uYWRkQW5pbWF0aW9uKDAsICdpZGxlJywgdHJ1ZSk7XHJcbiAgICB9XHJcbiAgICBtb3ZlTGVmdCgpIHtcclxuICAgICAgICB0aGlzLmFuaW0uc2V0QW5pbWF0aW9uKDAsICd3YWxrJywgZmFsc2UpO1xyXG4gICAgICAgIHRoaXMucGxheWVyLmdldENoaWxkQnlOYW1lKCdwbGF5ZXInKS5zY2FsZVggPSAtMC4xO1xyXG4gICAgICAgIC8vIHRoaXMucGxheWVyLmdldENvbXBvbmVudChjYy5SaWdpZEJvZHkpLmFwcGx5Rm9yY2VUb0NlbnRlcihjYy52MigyMCwwKSwgdHJ1ZSk7XHJcbiAgICAgICAgdGhpcy5wbGF5ZXIucnVuQWN0aW9uKGNjLnNlcXVlbmNlKGNjLm1vdmVCeSh0aGlzLmR1cmF0aW9uLCBjYy52MigtdGhpcy5tb3ZlU3BlZWQsIDApKSwgY2MuY2FsbEZ1bmMoKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmFuaW0uYWRkQW5pbWF0aW9uKDAsICdpZGxlJywgdHJ1ZSk7XHJcbiAgICAgICAgfSkpKTtcclxuICAgIH1cclxuICAgIG1vdmVSaWdodCgpIHtcclxuICAgICAgICB0aGlzLmFuaW0uc2V0QW5pbWF0aW9uKDAsICd3YWxrJywgZmFsc2UpO1xyXG4gICAgICAgIHRoaXMucGxheWVyLmdldENoaWxkQnlOYW1lKCdwbGF5ZXInKS5zY2FsZVggPSAwLjE7XHJcbiAgICAgICAgdGhpcy5wbGF5ZXIucnVuQWN0aW9uKGNjLnNlcXVlbmNlKGNjLm1vdmVCeSh0aGlzLmR1cmF0aW9uLCBjYy52Mih0aGlzLm1vdmVTcGVlZCwgMCkpLCBjYy5jYWxsRnVuYygoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuYW5pbS5hZGRBbmltYXRpb24oMCwgJ2lkbGUnLCB0cnVlKTtcclxuICAgICAgICB9KSkpO1xyXG4gICAgfVxyXG4gICAgbW92ZUp1bXAoKSB7XHJcbiAgICAgICAgaWYodGhpcy5pc0dyb3VuZCA9PSB0cnVlKXtcclxuICAgICAgICAgICAgdGhpcy5pc0dyb3VuZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLmFuaW0uc2V0QW5pbWF0aW9uKDAsICdqdW1wJywgZmFsc2UpXHJcbiAgICAgICAgICAgIGxldCBtb3ZlUG9zaXRpb247XHJcbiAgICAgICAgICAgIGlmKHRoaXMucGxheWVyLmdldENoaWxkQnlOYW1lKCdwbGF5ZXInKS5zY2FsZVggPiAwKXtcclxuICAgICAgICAgICAgICAgIG1vdmVQb3NpdGlvbiA9IHRoaXMubW92ZVNwZWVkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGVsc2V7XHJcbiAgICAgICAgICAgICAgICBtb3ZlUG9zaXRpb24gPSAtdGhpcy5tb3ZlU3BlZWQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5wbGF5ZXIucnVuQWN0aW9uKGNjLnNlcXVlbmNlKFxyXG4gICAgICAgICAgICBjYy5tb3ZlQnkodGhpcy5kdXJhdGlvbiwgY2MudjIobW92ZVBvc2l0aW9uLCB0aGlzLmp1bXBTcGVlZCkpLFxyXG4gICAgICAgICAgICBjYy5tb3ZlQnkodGhpcy5kdXJhdGlvbiwgY2MudjIobW92ZVBvc2l0aW9uLCAtIHRoaXMuanVtcFNwZWVkKSksXHJcbiAgICAgICAgICAgIGNjLmNhbGxGdW5jKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaXNHcm91bmQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hbmltLmFkZEFuaW1hdGlvbigwLCAnaWRsZScsIHRydWUpO1xyXG4gICAgICAgICAgICB9KSkpXHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2V7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBzZXRFdmVudExpc3Rlbm5lcigpIHtcclxuICAgICAgICB0aGlzLmFuaW0uc2V0U3RhcnRMaXN0ZW5lcigoZXZlbnQpID0+IHtcclxuICAgICAgICAgICAgY29uc3QgbmFtZSA9IGV2ZW50LmFuaW1hdGlvbi5uYW1lO1xyXG4gICAgICAgICAgICBzd2l0Y2ggKG5hbWUpIHtcclxuICAgICAgICAgICAgICAgIGNhc2UgJ3BvcnRhbCc6IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXkodGhpcy5wb3J0YWxTb3VuZCwgZmFsc2UsIDEpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sIDAuNSlcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXkodGhpcy5wb3J0YWxTb3VuZCwgZmFsc2UsIDEpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sIDIuNSlcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGNhc2UgJ3dhbGsnOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBjYXNlICdzaG9vdCc6IHtcclxuICAgICAgICAgICAgICAgICAgICBjYy5hdWRpb0VuZ2luZS5wbGF5KHRoaXMuc2hvb3RTb3VuZCwgZmFsc2UsIDEpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaW5pdEJ1bGxldCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgY2FzZSAnanVtcCc6e1xyXG4gICAgICAgICAgICAgICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXkodGhpcy5qdW1wU291bmQsIGZhbHNlLCAxKTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGNhc2UgJ2RlYXRoJzp7XHJcbiAgICAgICAgICAgICAgICAgICAgY2MuYXVkaW9FbmdpbmUucGxheSh0aGlzLmdhbWVPdmVyTXVzaWMsIGZhbHNlLCAxKTtcclxuICAgICAgICAgICAgICAgICAgICBjYy5zeXN0ZW1FdmVudC5vZmYoY2MuU3lzdGVtRXZlbnQuRXZlbnRUeXBlLktFWV9ET1dOLCB0aGlzLm9uS2V5RG93biwgdGhpcyk7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuICAgIGdhbWVPdmVyKCl7XHJcbiAgICAgICAgaWYodGhpcy5wbGF5ZXIueSA8IDY4KXtcclxuICAgICAgICAgICAgY2MuYXVkaW9FbmdpbmUucGxheSh0aGlzLmdhbWVPdmVyTXVzaWMsIGZhbHNlLCAxKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBcclxuICAgIFxyXG4gICAgLy8gdXBkYXRlIChkdCkge31cclxufVxyXG4iXX0=
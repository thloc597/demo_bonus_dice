"use strict";
cc._RF.push(module, '03374pJf5NI4r2wxTYyBB43', 'registerEvent');
// Scripts/registerEvent.ts

var EventEmitter = require('events');
var mEmitter = /** @class */ (function () {
    function mEmitter() {
        this._emiter = new EventEmitter();
        this._emiter.setMaxListeners(100);
    }
    mEmitter.prototype.emit = function () {
        var _a;
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        (_a = this._emiter).emit.apply(_a, args);
    };
    mEmitter.prototype.registerEvent = function (event, listener) {
        this._emiter.on(event, listener);
    };
    mEmitter.prototype.registerOnce = function (event, listener) {
        this._emiter.once(event, listener);
    };
    mEmitter.prototype.removeEvent = function (event, listener) {
        this._emiter.removeListener(event, listener);
    };
    mEmitter.prototype.destroy = function () {
        this._emiter.removeAllListeners();
        this._emiter = null;
        mEmitter.instance = null;
    };
    return mEmitter;
}());
mEmitter.instance = null;
module.exports = mEmitter;

cc._RF.pop();
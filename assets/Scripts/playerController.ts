// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class PlayerController extends cc.Component {
    @property(cc.AudioClip)
    gameOverMusic: cc.AudioClip = null;
    onBeginContact(contact, self, other) {
        cc.warn("OOPS", contact, self, other)
        if (other) {
            if(other.node.name == 'enemy'){
                this.node.getComponent(cc.RigidBody).enabledContactListener = false;
                this.node.getChildByName('player').getComponent(sp.Skeleton).setAnimation(0, 'death', false);
                this.scheduleOnce(()=>{
                    cc.audioEngine.play(this.gameOverMusic, false, 1);
                    this.node.destroy();
                }, 1.5)
               
            }
        }
    }
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        var manager = cc.director.getCollisionManager();
        manager.enabled = true;
        manager.enabledDebugDraw = true;
        manager.enabledDrawBoundingBox = true;
    }

    start () {

    }

    // update (dt) {}
}

// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import CoinController from "./coinController";
const { ccclass, property } = cc._decorator;

@ccclass
export default class GameController extends cc.Component {

    @property
    private moveSpeed: number;
    @property
    private countCoin: number;
    private posX = []
    private posLength: number;
    @property
    private bulletSpeed: number
    @property
    private jumpSpeed: number
    @property
    private duration: number
    @property(sp.Skeleton)
    anim: sp.Skeleton = null;
    @property(cc.Node)
    player: cc.Node = null;
    @property(cc.AudioClip)
    walkSound: cc.AudioClip = null;
    @property(cc.AudioClip)
    bgMusic: cc.AudioClip = null;
    @property(cc.AudioClip)
    coinSound: cc.AudioClip = null;
    @property(cc.AudioClip)
    touchSound: cc.AudioClip = null;
    @property(cc.AudioClip)
    shootSound: cc.AudioClip = null;
    @property(cc.AudioClip)
    jumpSound: cc.AudioClip = null;
    @property(cc.AudioClip)
    portalSound: cc.AudioClip = null;
    @property(cc.Prefab)
    bullet: cc.Prefab = null;
    @property(cc.Prefab)
    coin: cc.Prefab = null;
    @property(cc.Node)
    gameOverPopup: cc.Node = null;
    @property(cc.Node)
    parentLeft: cc.Node = null;
    @property(cc.Node)
    parentRight: cc.Node = null;
    private isGround: boolean = true;
    @property(cc.AudioClip)
    gameOverMusic: cc.AudioClip = null;
    // LIFE-CYCLE CALLBACKS:

    
    createArrPos(min: number, max: number){
        for(let i = min; i <= max; i+= 20){
            this.posX.push(i);
        }
        this.posLength = this.posX.length
        return this.posX.length;
    }
    onLoad() {
        cc.audioEngine.play(this.bgMusic, true, 0);
        this.createArrPos(-510, 470);
        this.setEventListenner()
        let physicsManager = cc.director.getPhysicsManager();
        physicsManager.enabled = true;
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        this.anim.setAnimation(0, 'portal', false);
        this.anim.addAnimation(0, 'idle', true);
        for(let i = 0; i < this.countCoin; i++){
            this.initCoin();
        }
        let manager = cc.director.getCollisionManager();
        manager.enabled = true;
        manager.enabledDebugDraw = true;
        this.anim.setEventListener((entry, event) => {
            const { data } = event;
            if(data.name == 'footstep'){
                cc.audioEngine.play(this.walkSound, false, 1);
            }

        });
    }
    start() {

    }
    
    onKeyDown(event) {
        switch (event.keyCode) {
            case cc.macro.KEY.left:
                this.moveLeft()
                break;
            case cc.macro.KEY.right:
                this.moveRight();
                break;
            case cc.macro.KEY.x:
                this.moveJump();
                break;
            case cc.macro.KEY.z:
                this.shoot();
                break;
        }
    }
    initCoin(){
        let posX = this.posX[(Math.floor(Math.random()*(this.posLength - 1 - 0 + 1)))];
        let posY = Math.floor(Math.random()*(-175 - -260 + 1)) + -260;
        let _coin = cc.instantiate(this.coin);
        _coin.parent = this.node;
        _coin.x = posX;
        _coin.y = posY;
    }
    initBullet() {
        cc.error("Init")
        let bullet = cc.instantiate(this.bullet);
        if (this.player.getChildByName('player').scaleX > 0) {
            bullet.parent = this.parentRight;
            bullet.runAction(cc.repeatForever(cc.moveBy(0.1, cc.v2(this.bulletSpeed, 0))));
        }
        else {
            bullet.parent = this.parentLeft;
            bullet.runAction(cc.repeatForever(cc.moveBy(0.1, cc.v2(-this.bulletSpeed, 0))));
        }

    }
    shoot() {
        this.anim.setAnimation(0, 'shoot', false);
        this.anim.addAnimation(0, 'idle', true);
    }
    moveLeft() {
        this.anim.setAnimation(0, 'walk', false);
        this.player.getChildByName('player').scaleX = -0.1;
        // this.player.getComponent(cc.RigidBody).applyForceToCenter(cc.v2(20,0), true);
        this.player.runAction(cc.sequence(cc.moveBy(this.duration, cc.v2(-this.moveSpeed, 0)), cc.callFunc(() => {
            this.anim.addAnimation(0, 'idle', true);
        })));
    }
    moveRight() {
        this.anim.setAnimation(0, 'walk', false);
        this.player.getChildByName('player').scaleX = 0.1;
        this.player.runAction(cc.sequence(cc.moveBy(this.duration, cc.v2(this.moveSpeed, 0)), cc.callFunc(() => {
            this.anim.addAnimation(0, 'idle', true);
        })));
    }
    moveJump() {
        if(this.isGround == true){
            this.isGround = false;
            this.anim.setAnimation(0, 'jump', false)
            let movePosition;
            if(this.player.getChildByName('player').scaleX > 0){
                movePosition = this.moveSpeed;
            }
            else{
                movePosition = -this.moveSpeed;
            }
            this.player.runAction(cc.sequence(
            cc.moveBy(this.duration, cc.v2(movePosition, this.jumpSpeed)),
            cc.moveBy(this.duration, cc.v2(movePosition, - this.jumpSpeed)),
            cc.callFunc(() => {
                this.isGround = true;
                this.anim.addAnimation(0, 'idle', true);
            })))
        }
        else{
            return;
        }
    }
    setEventListenner() {
        this.anim.setStartListener((event) => {
            const name = event.animation.name;
            switch (name) {
                case 'portal': {
                    this.scheduleOnce(() => {
                        cc.audioEngine.play(this.portalSound, false, 1);
                    }, 0.5)
                    this.scheduleOnce(() => {
                        cc.audioEngine.play(this.portalSound, false, 1);
                    }, 2.5)
                    break;
                }
                case 'walk': {
                    break;
                }
                case 'shoot': {
                    cc.audioEngine.play(this.shootSound, false, 1);
                    this.initBullet();
                    break;
                }
                case 'jump':{
                    cc.audioEngine.play(this.jumpSound, false, 1);
                    break;
                }
                case 'death':{
                    cc.audioEngine.play(this.gameOverMusic, false, 1);
                    cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
                    break;
                }
            }
        });
    }
    gameOver(){
        if(this.player.y < 68){
            cc.audioEngine.play(this.gameOverMusic, false, 1);
        }
    }
    
    
    // update (dt) {}
}

"use strict";
cc._RF.push(module, 'bda65V044ZBJrYIaWWvREjR', 'gameController');
// Scripts/gameController.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var GameController = /** @class */ (function (_super) {
    __extends(GameController, _super);
    function GameController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.posX = [];
        _this.anim = null;
        _this.player = null;
        _this.walkSound = null;
        _this.bgMusic = null;
        _this.coinSound = null;
        _this.touchSound = null;
        _this.shootSound = null;
        _this.jumpSound = null;
        _this.portalSound = null;
        _this.bullet = null;
        _this.coin = null;
        _this.gameOverPopup = null;
        _this.parentLeft = null;
        _this.parentRight = null;
        _this.isGround = true;
        _this.gameOverMusic = null;
        return _this;
        // update (dt) {}
    }
    // LIFE-CYCLE CALLBACKS:
    GameController.prototype.createArrPos = function (min, max) {
        for (var i = min; i <= max; i += 20) {
            this.posX.push(i);
        }
        this.posLength = this.posX.length;
        return this.posX.length;
    };
    GameController.prototype.onLoad = function () {
        var _this = this;
        cc.audioEngine.play(this.bgMusic, true, 0);
        this.createArrPos(-510, 470);
        this.setEventListenner();
        var physicsManager = cc.director.getPhysicsManager();
        physicsManager.enabled = true;
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        this.anim.setAnimation(0, 'portal', false);
        this.anim.addAnimation(0, 'idle', true);
        for (var i = 0; i < this.countCoin; i++) {
            this.initCoin();
        }
        var manager = cc.director.getCollisionManager();
        manager.enabled = true;
        manager.enabledDebugDraw = true;
        this.anim.setEventListener(function (entry, event) {
            var data = event.data;
            if (data.name == 'footstep') {
                cc.audioEngine.play(_this.walkSound, false, 1);
            }
        });
    };
    GameController.prototype.start = function () {
    };
    GameController.prototype.onKeyDown = function (event) {
        switch (event.keyCode) {
            case cc.macro.KEY.left:
                this.moveLeft();
                break;
            case cc.macro.KEY.right:
                this.moveRight();
                break;
            case cc.macro.KEY.x:
                this.moveJump();
                break;
            case cc.macro.KEY.z:
                this.shoot();
                break;
        }
    };
    GameController.prototype.initCoin = function () {
        var posX = this.posX[(Math.floor(Math.random() * (this.posLength - 1 - 0 + 1)))];
        var posY = Math.floor(Math.random() * (-175 - -260 + 1)) + -260;
        var _coin = cc.instantiate(this.coin);
        _coin.parent = this.node;
        _coin.x = posX;
        _coin.y = posY;
    };
    GameController.prototype.initBullet = function () {
        cc.error("Init");
        var bullet = cc.instantiate(this.bullet);
        if (this.player.getChildByName('player').scaleX > 0) {
            bullet.parent = this.parentRight;
            bullet.runAction(cc.repeatForever(cc.moveBy(0.1, cc.v2(this.bulletSpeed, 0))));
        }
        else {
            bullet.parent = this.parentLeft;
            bullet.runAction(cc.repeatForever(cc.moveBy(0.1, cc.v2(-this.bulletSpeed, 0))));
        }
    };
    GameController.prototype.shoot = function () {
        this.anim.setAnimation(0, 'shoot', false);
        this.anim.addAnimation(0, 'idle', true);
    };
    GameController.prototype.moveLeft = function () {
        var _this = this;
        this.anim.setAnimation(0, 'walk', false);
        this.player.getChildByName('player').scaleX = -0.1;
        // this.player.getComponent(cc.RigidBody).applyForceToCenter(cc.v2(20,0), true);
        this.player.runAction(cc.sequence(cc.moveBy(this.duration, cc.v2(-this.moveSpeed, 0)), cc.callFunc(function () {
            _this.anim.addAnimation(0, 'idle', true);
        })));
    };
    GameController.prototype.moveRight = function () {
        var _this = this;
        this.anim.setAnimation(0, 'walk', false);
        this.player.getChildByName('player').scaleX = 0.1;
        this.player.runAction(cc.sequence(cc.moveBy(this.duration, cc.v2(this.moveSpeed, 0)), cc.callFunc(function () {
            _this.anim.addAnimation(0, 'idle', true);
        })));
    };
    GameController.prototype.moveJump = function () {
        var _this = this;
        if (this.isGround == true) {
            this.isGround = false;
            this.anim.setAnimation(0, 'jump', false);
            var movePosition = void 0;
            if (this.player.getChildByName('player').scaleX > 0) {
                movePosition = this.moveSpeed;
            }
            else {
                movePosition = -this.moveSpeed;
            }
            this.player.runAction(cc.sequence(cc.moveBy(this.duration, cc.v2(movePosition, this.jumpSpeed)), cc.moveBy(this.duration, cc.v2(movePosition, -this.jumpSpeed)), cc.callFunc(function () {
                _this.isGround = true;
                _this.anim.addAnimation(0, 'idle', true);
            })));
        }
        else {
            return;
        }
    };
    GameController.prototype.setEventListenner = function () {
        var _this = this;
        this.anim.setStartListener(function (event) {
            var name = event.animation.name;
            switch (name) {
                case 'portal': {
                    _this.scheduleOnce(function () {
                        cc.audioEngine.play(_this.portalSound, false, 1);
                    }, 0.5);
                    _this.scheduleOnce(function () {
                        cc.audioEngine.play(_this.portalSound, false, 1);
                    }, 2.5);
                    break;
                }
                case 'walk': {
                    break;
                }
                case 'shoot': {
                    cc.audioEngine.play(_this.shootSound, false, 1);
                    _this.initBullet();
                    break;
                }
                case 'jump': {
                    cc.audioEngine.play(_this.jumpSound, false, 1);
                    break;
                }
                case 'death': {
                    cc.audioEngine.play(_this.gameOverMusic, false, 1);
                    cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, _this.onKeyDown, _this);
                    break;
                }
            }
        });
    };
    GameController.prototype.gameOver = function () {
        if (this.player.y < 68) {
            cc.audioEngine.play(this.gameOverMusic, false, 1);
        }
    };
    __decorate([
        property
    ], GameController.prototype, "moveSpeed", void 0);
    __decorate([
        property
    ], GameController.prototype, "countCoin", void 0);
    __decorate([
        property
    ], GameController.prototype, "bulletSpeed", void 0);
    __decorate([
        property
    ], GameController.prototype, "jumpSpeed", void 0);
    __decorate([
        property
    ], GameController.prototype, "duration", void 0);
    __decorate([
        property(sp.Skeleton)
    ], GameController.prototype, "anim", void 0);
    __decorate([
        property(cc.Node)
    ], GameController.prototype, "player", void 0);
    __decorate([
        property(cc.AudioClip)
    ], GameController.prototype, "walkSound", void 0);
    __decorate([
        property(cc.AudioClip)
    ], GameController.prototype, "bgMusic", void 0);
    __decorate([
        property(cc.AudioClip)
    ], GameController.prototype, "coinSound", void 0);
    __decorate([
        property(cc.AudioClip)
    ], GameController.prototype, "touchSound", void 0);
    __decorate([
        property(cc.AudioClip)
    ], GameController.prototype, "shootSound", void 0);
    __decorate([
        property(cc.AudioClip)
    ], GameController.prototype, "jumpSound", void 0);
    __decorate([
        property(cc.AudioClip)
    ], GameController.prototype, "portalSound", void 0);
    __decorate([
        property(cc.Prefab)
    ], GameController.prototype, "bullet", void 0);
    __decorate([
        property(cc.Prefab)
    ], GameController.prototype, "coin", void 0);
    __decorate([
        property(cc.Node)
    ], GameController.prototype, "gameOverPopup", void 0);
    __decorate([
        property(cc.Node)
    ], GameController.prototype, "parentLeft", void 0);
    __decorate([
        property(cc.Node)
    ], GameController.prototype, "parentRight", void 0);
    __decorate([
        property(cc.AudioClip)
    ], GameController.prototype, "gameOverMusic", void 0);
    GameController = __decorate([
        ccclass
    ], GameController);
    return GameController;
}(cc.Component));
exports.default = GameController;

cc._RF.pop();